// var canvas = document.getElementById('canvas');
// var ctx = canvas.getContext('2d');

// var data = '<svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">' +
//   '<foreignObject width="100%" height="100%">' +
//   '<div xmlns="http://www.w3.org/1999/xhtml" style="font-size:40px">' +
//   '<em>I</em> like' +
//   '<span style="color:white; text-shadow:0 0 2px blue;">' +
//   'cheese</span>' +
//   '</div>' +
//   '</foreignObject>' +
//   '</svg>';

// var DOMURL = window.URL || window.webkitURL || window;

// var img = new Image();
// var svg = new Blob([data], {
//   type: 'image/svg+xml;charset=utf-8'
// });
// var url = DOMURL.createObjectURL(svg);

// img.onload = function () {
//   ctx.drawImage(img, 0, 0);
//   DOMURL.revokeObjectURL(url);
// }

// img.src = url;
import yoga, {
  Node
} from 'yoga-layout';
import { runInNewContext } from 'vm';

var canvas = document.querySelector('.canvas');
const elemLeft = canvas.offsetLeft
const elemTop = canvas.offsetTop

var ctx = canvas.getContext('2d');
const getNode = () => {
  const node = Node.create()
  node.setWidth(30)
  node.setHeight(50)
  node.setMargin(yoga.EDGE_ALL, 5)
  return node
}

let seats = new Array(100 * 100).fill("")
seats = seats.map(s => {
  return {
    node: null,
    style: null,
    choosed: false
  }
})
console.log(yoga)
// const wrap = Node.create()
// wrap.setWidth(750)
// wrap.setHeight(1200)

const wrap = Node.create()
wrap.setWidth(375)
wrap.setHeight(12000)
wrap.setDisplay(yoga.DISPLAY_FLEX)
wrap.setFlexDirection(yoga.FLEX_DIRECTION_ROW)
// wrap.setJustifyContent(yoga.JUSTIFY_CENTER)
wrap.setFlexWrap(yoga.WRAP_WRAP)

seats.forEach((s,index) => {
  s.node = getNode();
  wrap.insertChild(s.node, index)
})
const draw = () => {
  seats.forEach((s, index) => {
    let style = null
    if (s.style) {
      style = s.style
    } else {
      s.style = style = s.node.getComputedLayout()
    }
    ctx.fillStyle = s.choosed ? "#ff803a" : "#ccc"
    ctx.fillText(index + 1, style.left, style.top)
    ctx.fillRect(style.left, style.top, style.width, style.height)
    ctx.strokeRect(style.left, style.top, style.width, style.height)
  })
}
wrap.calculateLayout(375, 120000, yoga.DIRECTION_LTR)
// ctx.strokeRect(20, 20, 30, 50)
// ctx.fillRect(20, 20, 30, 50)
// ctx.strokeRect(20 + 30 + 10, 20 + 50 + 10, 30, 50)
// ctx.fillRect(20 + 30 + 10, 20 + 50 + 10, 30, 50)

 draw()


 document.querySelector(".scroll-mask").addEventListener('click', (event) => {
  var x = event.pageX - elemLeft,
    y = document.querySelector(".container").scrollTop + event.pageY - elemTop;
    //document.querySelector(".container").scrollTop +
    console.log(document.querySelector(".container").scrollTop, event.pageY, document.querySelector(".container").scrollTop + event.pageY - elemTop)
  const seat = seats.find((element) => {
    const style = element.style
    if (y > style.top && y < style.top + style.height &&
      x > style.left && x < style.left + style.width) {
      return true
    }
  })
  if (!seat) return 
  const style = seat.style
  if (seat.choosed) {
    ctx.fillStyle = "#ccc"
    seat.choosed = false
  } else {
    ctx.fillStyle = "#ff803a"
    seat.choosed = true
  }
  ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.save();
  draw()
  ctx.restore()
  // ctx.fillRect(style.left, style.top, style.width, style.height)
  // ctx.strokeRect(style.left, style.top, style.width, style.height)
  console.log(seat)
})

 document.querySelector(".container").addEventListener('scroll', (e) => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.save();
 
  ctx.translate(0, -e.target.scrollTop)
   draw()
  ctx.restore();
 }, false)



var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
stats.domElement.style.cssText = 'position:fixed;top:0px;left:0px;z-index:5';
document.body.appendChild(stats.dom);
setInterval(function () {
  stats.update()

}, 1000 / 60);