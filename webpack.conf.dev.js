module.exports = {
  mode: 'development',
  entry: {
    main: './src/index.js'
  },  
  devServer: {
    contentBase: './',
    compress: true,
    port: 9000
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: ["./src"]
      }
    ]
  },
  devtool: '#cheap-module-eval-source-map'
}